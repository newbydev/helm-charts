#!/usr/bin/env sh

find . -name "*.tar.gz" -print0 | while read -r -d $'\0' file
  do
    curl \
      --request POST \
      --user gitlab-ci-token:"$CI_JOB_TOKEN" \
      --form "chart=@$file" \
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
  done
exit 0
