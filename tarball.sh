#!/usr/bin/env bash

find ./charts -type d -print0 -mindepth 1 -maxdepth 1 | while read -r -d $'\0' dir
  do
    name=$(basename "$dir")
    tar -C "$dir" -czvf "$name.tar.gz" .
  done
exit 0
