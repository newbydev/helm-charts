# cert-manager CRDs

[cert-manager CRDs](https://cert-manager.io) - Jetstack cert-manager CRDs

## Introduction

This chart deploys cert-manager crds on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.16+

## Installing the Chart

```
repositories:
  - name: newbydev
    url: https://gitlab.com/api/v4/projects/38260926/packages/helm/stable
  - name: jetstack
    url: https://charts.jetstack.io
    
templates:
  default: &default
    namespace: "{{`{{ .Release.Name }}`}}"
    values:
      - ./values/{{`{{ .Release.Name }}`}}.yaml.gotmpl
    missingFileHandler: Info

releases:
  - name: cert-manager-crds
    chart: newbydev/cert-manager-crds
    version: v1.5.3
    <<: *default

  - name: cert-manager
    chart: jetstack/cert-manager
    namespace: cert-manager
    version: v1.5.3
```


